package com.jun.easy.util;

import java.util.Random;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommonUtil {

	public static String getRandomStringByOption(int startPoint, int cnt, int length) {
		String secretKey = new String();
		Random rnd = new Random();
		for(int i=0; i<length; i++) {
			secretKey += (char)((int)rnd.nextInt(cnt) + startPoint);
		}
		return secretKey;
	}
	
	public static String getCookie(HttpServletRequest req, String key) {
		Cookie[] cookies = req.getCookies();
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				String[] cookieKeyValue = {cookie.getName(), cookie.getValue()};
				if(cookieKeyValue[0].equals(key)) return cookieKeyValue[1];
			}
		}
		return null;
	}
	
	public static void setCookie(HttpServletResponse res, String... keyValue) {
		Cookie cookie = new Cookie(keyValue[0], keyValue[1]);
		cookie.setMaxAge(60*60*24);
		cookie.setPath("/");
		res.addCookie(cookie);
	}
	
	public static void removeCookie(HttpServletResponse res, String key) {
		Cookie cookie = new Cookie(key, "");
		cookie.setMaxAge(0);
		cookie.setPath("/");
		res.addCookie(cookie);
	}
	
	public static boolean getIsCommonRequest(HttpServletRequest req) {
		String[] splitedStr = Config.COMMON_URI_LIST.split(",");
		for(String str : splitedStr) {
			if(req.getRequestURI().contains(str)) return true;
		}
		return false;
	}
}