package com.jun.easy.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jun.easy.exception.ServerError;
import com.jun.easy.exception.ServerException;
import com.jun.easy.util.CommonUtil;
import com.jun.easy.util.Config;
import com.jun.easy.util.CryptUtil;

public class Auth extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler)
			throws Exception {
		System.out.println("URL : " + req.getRequestURL());
		
		String origin = req.getHeader("Origin");
		if(origin != null) {
			res.setHeader("Access-Control-Allow-Origin", origin);
			res.setHeader("Access-Control-Allow-Credentials", "true");
			res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			res.setHeader("Access-Control-Max-Age", "3600");
			res.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		}
		
		if(CommonUtil.getIsCommonRequest(req)) return true;
		String authCookieValue = CommonUtil.getCookie(req, Config.COOKIE_KEY_USER_AUTH);
		if(authCookieValue != null) {
			req.setAttribute(Config.CLIENT_ID, Integer.parseInt(CryptUtil.Decrypt(Config.CRYPT_KEY_USER, authCookieValue).split("&")[0]));
			return true;
		}else throw new ServerException(ServerError.ERR_COMMON_GENERAL_AUTH_DENIED);
	}
}
