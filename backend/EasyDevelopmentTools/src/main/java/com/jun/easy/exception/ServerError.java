package com.jun.easy.exception;

public enum ServerError {
	
	ERR_NOERR(0, ""),
	
	ERR_COMMON_GENERAL(0x0100, "요청에 실패했습니다."),
	ERR_COMMON_GENERAL_AUTH_DENIED(ERR_COMMON_GENERAL.getCode() + 1, "인증이 유효하지 않습니다."),
	ERR_COMMON_GENERAL_DATABASE(ERR_COMMON_GENERAL_AUTH_DENIED.getCode() + 1, "정상적으로 저장하지 못했습니다."),
	
	ERR_USER_ALREADY_REGISTERED_ACCOUNT(0x0200, "이미 존재하는 아이디 입니다."),
	ERR_USER_UNREGISTERED_ACCOUNT(ERR_USER_ALREADY_REGISTERED_ACCOUNT.getCode() + 1, "계정이 존재하지 않습니다."),
	ERR_USER_UNMATCHED_PASSWORD(ERR_USER_UNREGISTERED_ACCOUNT.getCode() + 1, "비밀번호가 일치하지 않습니다."),
	;
	
	private int code;
	private String msg;
	
	ServerError(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

}
