package com.jun.easy.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jun.easy.bean.model.ApiWithBLOBs;
import com.jun.easy.bean.model.Data;
import com.jun.easy.bean.model.ProjectWithBLOBs;
import com.jun.easy.bean.model.User;
import com.jun.easy.util.DataMap;

public interface UserServiceMapper {
	List<User> selectUserBySelective(User user);
	List<ProjectWithBLOBs> selectProjectBySelective(ProjectWithBLOBs project);
	List<ApiWithBLOBs> selectApiBySelective(ApiWithBLOBs api);
	List<ProjectWithBLOBs> projectList(@Param("userId") int userId, @Param("offset") int offset, @Param("rowCnt") int rowCnt, @Param("isWholeSelect") boolean isWholeSelect);
	List<ProjectWithBLOBs> projectList(@Param("userId") int userId, @Param("isWholeSelect") boolean isWholeSelect);
	List<ApiWithBLOBs> apiList(@Param("projectId") int projectId, @Param("offset") int offset, @Param("rowCnt") int rowCnt);
	int projectListCnt(@Param("userId") int userId);
	int apiListCnt(@Param("projectId") int projectId);
	List<Data> dataList(@Param("userId") int userId);
	List<Integer> countProjectApiInsertDate(@Param("userId") int userId);
	List<Integer> countDataInsertDate(@Param("userId") int userId);
}
