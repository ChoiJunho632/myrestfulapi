package com.jun.easy.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jun.easy.bean.model.Api;
import com.jun.easy.bean.model.ApiWithBLOBs;
import com.jun.easy.bean.model.Data;
import com.jun.easy.bean.model.Project;
import com.jun.easy.bean.model.ProjectWithBLOBs;
import com.jun.easy.bean.model.User;
import com.jun.easy.exception.ServerError;
import com.jun.easy.exception.ServerException;
import com.jun.easy.service.UserService;
import com.jun.easy.util.CommonUtil;
import com.jun.easy.util.Config;
import com.jun.easy.util.CryptUtil;
import com.jun.easy.util.ResponseData;

@RestController
@RequestMapping(value="user/", headers="Accept=application/json")
public class UserController {

	@Autowired
	private HttpServletRequest req;
	
	@Autowired
	private UserService userService;
	
	private int getClientId() {
		return (int)req.getAttribute(Config.CLIENT_ID);
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value=HttpStatus.OK)
	@ResponseBody
	public ResponseData handleException(Exception e) {
		ResponseData resp = new ResponseData(ServerError.ERR_COMMON_GENERAL, e.getMessage());
		return resp;
	}
	
	@ExceptionHandler(ServerException.class)
	@ResponseStatus(value=HttpStatus.OK)
	@ResponseBody
	public ResponseData handleServerException(ServerException e) {
		ResponseData resp = new ResponseData(e);
		return resp;
	}
	
	@RequestMapping(value="register", method=RequestMethod.POST)
	public @ResponseBody ResponseData register(
			User user) throws Exception {
		ResponseData resd = new ResponseData(userService.register(user));
		return resd;
	}
	
	@RequestMapping(value="login", method=RequestMethod.POST)
	public @ResponseBody ResponseData login(
			HttpServletResponse res
			, User user
			, @RequestParam("isAutoLogin") boolean isAutoLogin) throws Exception {
		ResponseData resd = new ResponseData(userService.login(user));
		CommonUtil.setCookie(res, Config.COOKIE_KEY_USER_AUTH, (String)((ResponseData)resd.get("data")).get("auth"));
		if(!isAutoLogin) ((ResponseData)resd.get("data")).remove("auth");
		return resd;
	}
	
	@RequestMapping(value="logout", method=RequestMethod.GET)
	public @ResponseBody ResponseData logout(
			HttpServletResponse res) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		CommonUtil.removeCookie(res, Config.COOKIE_KEY_USER_AUTH);
		return resd;
	}
	
	@RequestMapping(value="autoLogin", method=RequestMethod.POST)
	public @ResponseBody ResponseData autoLogin(
			HttpServletResponse res
			, @RequestParam("auth") String cryptAuthString) throws Exception {
		ResponseData resd = new ResponseData(userService.autoLogin(cryptAuthString));
		CommonUtil.setCookie(res, Config.COOKIE_KEY_USER_AUTH, (String)((ResponseData)resd.get("data")).get("auth"));
		((ResponseData)resd.get("data")).remove("auth");
		return resd;
	}
	
	@RequestMapping(value="project/list", method=RequestMethod.GET)
	public @ResponseBody ResponseData projectList(
			@RequestParam("offset") int listOffset
			, @RequestParam("rowCnt") int rowCnt) throws Exception {
		ResponseData resd = new ResponseData(userService.projectList(getClientId(), listOffset, rowCnt));
		return resd;
	}
	
	@RequestMapping(value="project/list/whole", method=RequestMethod.GET)
	public @ResponseBody ResponseData projectList() throws Exception {
		ResponseData resd = new ResponseData(userService.projectList(getClientId(), Config.NONE, Config.NONE));
		return resd;
	}
	
	@RequestMapping(value="project/create", method=RequestMethod.POST)
	public @ResponseBody ResponseData projectCreate(
			ProjectWithBLOBs project) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.projectCreate(project, getClientId());
		return resd;
	}
	
	@RequestMapping(value="project/update", method=RequestMethod.POST)
	public @ResponseBody ResponseData projectUpdate(
			ProjectWithBLOBs project) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.projectUpdate(project, getClientId());
		return resd;
	}
	
	@RequestMapping(value="project/delete", method=RequestMethod.GET)
	public @ResponseBody ResponseData projectUpdate(
			@RequestParam("projectId") int projectId) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.projectDelete(projectId, getClientId());
		return resd;
	}
	
	@RequestMapping(value="api/list", method=RequestMethod.GET)
	public @ResponseBody ResponseData apiList(
			@RequestParam("projectId") int projectId
			, @RequestParam("offset") int listOffset
			, @RequestParam("rowCnt") int rowCnt) throws Exception {
		ResponseData resd = new ResponseData(userService.apiList(projectId, listOffset, rowCnt));
		return resd;
	}
	
	@RequestMapping(value="api/create", method=RequestMethod.POST)
	public @ResponseBody ResponseData apiCreate(
			ApiWithBLOBs api) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.apiCreate(api);
		return resd;
	}
	
	@RequestMapping(value="api/update", method=RequestMethod.POST)
	public @ResponseBody ResponseData apiUpdate(
			ApiWithBLOBs api) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.apiUpdate(api);
		return resd;
	}
	
	@RequestMapping(value="api/delete", method=RequestMethod.GET)
	public @ResponseBody ResponseData apiDelete(
			@RequestParam("apiId") int apiId
			, @RequestParam("projectId") int projectId) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.apiDelete(apiId, projectId);
		return resd;
	}
	
	@RequestMapping(value="data/create", method=RequestMethod.POST)
	public @ResponseBody ResponseData dataCreate(
			Data data) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.dataCreate(data, getClientId());
		return resd;
	}
	
	@RequestMapping(value="data/update", method=RequestMethod.POST)
	public @ResponseBody ResponseData dataUpdate(
			Data data) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.dataUpdate(data);
		return resd;
	}
	
	@RequestMapping(value="data/delete", method=RequestMethod.GET)
	public @ResponseBody ResponseData dataDelete(
			int dataId) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.dataDelete(dataId);
		return resd;
	}
	
	@RequestMapping(value="data/list", method=RequestMethod.GET)
	public @ResponseBody ResponseData dataList() throws Exception {
		ResponseData resd = new ResponseData(userService.dataList(getClientId()));
		return resd;
	}
	
	@RequestMapping(value="graphData", method=RequestMethod.GET)
	public @ResponseBody ResponseData projectInsertCount() throws Exception {
		ResponseData resd = new ResponseData(userService.insertDateCountForGraph(getClientId()));
		return resd;
	}
	
	@RequestMapping(value="findAccount", method=RequestMethod.POST)
	public @ResponseBody ResponseData findAccount(
			User user) throws Exception {
		ResponseData resd = new ResponseData(userService.findAccount(user));
		return resd;
	}
	
	@RequestMapping(value="findPassword", method=RequestMethod.POST)
	public @ResponseBody ResponseData findPassword(
			User user) throws Exception {
		ResponseData resd = new ResponseData(userService.findPassword(user));
		return resd;
	}
	
	@RequestMapping(value="myInfo/passwordConfirm", method=RequestMethod.POST)
	public @ResponseBody ResponseData myInfoPasswordConfirm(
			User user) throws Exception {
		ResponseData resd = new ResponseData(userService.myInfoPasswordConfirm(user, getClientId()));
		return resd;
	}
	
	@RequestMapping(value="myInfo/update", method=RequestMethod.POST)
	public @ResponseBody ResponseData myInfoUpdate(User user) throws Exception {
		user.setUserId(getClientId());
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		userService.myInfoUpdate(user);
		return resd;
	}
	
	@RequestMapping(value="myInfo/passwordChange", method=RequestMethod.POST)
	public @ResponseBody ResponseData myInfoPasswordChange(User user) throws Exception {
		ResponseData resd = new ResponseData(ServerError.ERR_NOERR);
		user.setUserId(getClientId());
		userService.myInfoPasswordChange(user);
		return resd;
	}
	
}
