package com.jun.easy.util;

public class Config {

	public static final String CRYPT_KEY_USER = "EASYdevelopmentTOOLS@USER";
	
	public static final String COOKIE_KEY_USER_AUTH = "user_d";
	
	public static final String AUTH_LEVEL_USER_DEFAULT = "user";
	public static final String AUTH_LEVEL_ADMIN_DEFAULT = "admin";
	
	public static final String COMMON_URI_LIST = "login,logout,register,find";
	
	public static final int NONE = -1;
	
	public static final String CLIENT_ID = "client_id";
	
	public static final int QUERY_SELECT_LIMIT_OFFSET_DEFAULT = 0;
	
	public static final int RANDOM_PASSWORD_CNT = 26;
	public static final int RANDOM_PASSWORD_START_POINT = 65;
	public static final int RANDOM_PASSWORD_LENGTH = 15;
}
