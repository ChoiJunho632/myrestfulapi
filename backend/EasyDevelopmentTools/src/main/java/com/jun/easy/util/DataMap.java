package com.jun.easy.util;

import java.math.BigDecimal;
import java.security.Timestamp;
import java.util.HashMap;
import java.util.List;

public class DataMap extends HashMap<String, Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Object getDateMap(String key) {
		if(this.containsKey(key)) return (DataMap)this.get(key);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<DataMap> getList(String key) {
		if(this.containsKey(key)) return (List<DataMap>)this.get(key);
		return null;
	}
	
	public void addInt(String key, int value) {
		this.put(key, this.getInt(key) + value);
	}
	
	public void addLong(String key, long value) {
		this.put(key, this.getLong(key) + value);
	}
	
	public int getInt(String key) {
		return getInt(key, 0);
	}
	
	public int getInt(String key, int default_value) {
		if(this.containsKey(key)) {
			if(this.get(key) instanceof BigDecimal) {
				BigDecimal value = (BigDecimal)(this.get(key));
				return value.intValue();
			}else if(this.get(key) instanceof Float) {
				Float value = (Float)(this.get(key));
				return value.intValue();
			}else if(this.get(key) instanceof Long) {
				Long value = (Long)(this.get(key));
				return value.intValue();
			}
			return (Integer)this.get(key);
		}
		return default_value;
	}
	
	public Timestamp getTimestamp(String key) {
		return (Timestamp)this.get(key);
	}
	
	public Long getLong(String key) {
		return getLong(key, 0);
	}
	
	public long getLong(String key, long default_value){
		if(this.containsKey(key)){
			if (this.get(key) instanceof BigDecimal){
				BigDecimal value =(BigDecimal)(this.get(key));
				return value.longValue();
			}
			if (this.get(key) instanceof Integer){
				Integer value = (Integer)(this.get(key));
				return value.longValue();
			}
			return (Long)this.get(key);
		}
		return default_value;
	}
	
	public void addFloat(String key, float value){
		this.put(key, this.getFloat(key) + value);
	}
	
	public float getFloat(String key){
		return getFloat(key, 0);
	}
	
	public float getFloat(String key, float default_value){
		if(this.containsKey(key)){
			if (this.get(key) instanceof BigDecimal){
				BigDecimal value =(BigDecimal)(this.get(key));
				return value.floatValue();
			}
			return (Float)this.get(key);
		}
		return default_value;
	}
	
	public String getString(String key){
		return getString(key, "");
	}

	public String getString(String key, String default_value){
		if(this.containsKey(key)){
			return (String)this.get(key);
		}
		return default_value;
	}
	
}
