package com.jun.easy.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.jun.easy.util.Config;

public class CryptUtil {

	public static String Encrypt(String pKey, String pContent) {
		DESKeySpec desKeySpec;
		Key key = null;
		try {
			desKeySpec = new DESKeySpec(pKey.getBytes());
			
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			key = keyFactory.generateSecret(desKeySpec);
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			
			byte[] inputBytes1 = pContent.getBytes("UTF8");
			byte[] outputBytes1 = cipher.doFinal(inputBytes1);
			Decrypt(pKey, new String(Base64.encodeBase64(outputBytes1)));
			return new String(Base64.encodeBase64(outputBytes1));
		}catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public static String Decrypt(String pKey, String pContent) {
		DESKeySpec desKeySpec;
		Key key = null;
		try {
			desKeySpec = new DESKeySpec(pKey.getBytes());
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			key = keyFactory.generateSecret(desKeySpec);
			cipher.init(Cipher.DECRYPT_MODE, key);
			
			byte[] inputBytes1 = Base64.decodeBase64(pContent);
			byte[] outputBytes1 = cipher.doFinal(inputBytes1);
			return new String(outputBytes1, "UTF8");
		}catch(Exception e) {
			return null;
		}
	}
	
	public static void main(String[] args) {
		String temp = CryptUtil.Encrypt(Config.CRYPT_KEY_USER, "?Wj/G[x0(&");
	}
}
