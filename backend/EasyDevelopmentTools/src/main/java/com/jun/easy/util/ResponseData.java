package com.jun.easy.util;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.jun.easy.exception.ServerError;
import com.jun.easy.exception.ServerException;

@Component
public class ResponseData extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5978894719282261695L;

	private static final String CODE = "code";
	private static final String ERRMSG = "errmsg";
	private static final String ERRMSGV = "errmsgvariable";
	private static final String KEY = "key";
	private final String DATA = "data";
	
	private int code;
	private String errmsg;
	private String key;
	
	public ResponseData() {
		super();
	}
	
	private ResponseData(int code, String errmsg, String key) {
		super();
		setCode(code);
		setErrmsg(errmsg);
		setKey(key);
	}
	
	public ResponseData(ServerError err) {
		this(err.getCode(), err.getMsg(), err.name());
	}
	
	public ResponseData(ServerException e) {
		this(e.getCode(), e.getErrmsg() , e.getKey());
		put(ERRMSGV, e.getVariable());
		
		if(e.getVariable() != null)
			setErrmsg(String.format(e.getErrmsg(), e.getVariable()));
	}
	
	public ResponseData(ServerError err, String variable) {
		this(err);
		put(ERRMSGV, variable);
	}
	
	public ResponseData(HashMap<String, Object> data) {
		super();
		this.code = ServerError.ERR_NOERR.getCode();
		this.errmsg = ServerError.ERR_NOERR.getMsg();
		put(CODE, this.code);
		put(ERRMSG, this.errmsg);
		put(ERRMSGV, null);
		put(KEY, ServerError.ERR_NOERR.name());
		put(DATA, data);
	}

	private void setCode(int code) {
		this.code = code;
		put(CODE, code);
	}
	
	private void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
		put(ERRMSG, errmsg);
	}
	
	
	public void setError(ServerError err){
		setCode(err.getCode());
		setErrmsg(err.getMsg());
	}

	public void setError(ServerError err, String variable){
		setError(err.getCode(), err.getMsg(), variable);
	}

	public void setError(ServerError err, String ... variables){
		setError(err.getCode(), err.getMsg(), variables);
	}

	private void setError(int code, String errmsg, String variable){
		setCode(code);
		setErrmsg(errmsg);
		put(ERRMSGV, variable);
	}
	
	private void setError(int code, String errmsg, String ... variables){
		setCode(code);
		setErrmsg(errmsg);
		put(ERRMSGV, variables);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getCode() {
		return code;
	}

	public String getErrmsg() {
		return errmsg;
	}
	
}
