package com.jun.easy.exception;

import com.jun.easy.exception.ServerError;

public class ServerException extends Exception {

	private static final long serialVersionUID = -5539492955563883150L;
	
	private int code;
	private String errmsg;
	private String variable;
	private String key;
	
	public ServerException(int code) {
		super();
		this.setCode(code);
	}
	
	public ServerException(int code, String errmsg, String key) {
		super();
		this.setCode(code);
		this.setErrmsg(errmsg);
		this.setKey(key);
	}
	
	public ServerException(Exception e, int code) {
		super(e);
		this.setCode(code);
	}
	
	public ServerException(Exception e, int code, String errmsg) {
		super(e);
		this.setCode(code);
		this.setErrmsg(errmsg);
		this.setKey("");
	}
	
	public ServerException(Exception e, ServerError err, String errmsg) {
		super(e);
		this.setCode(err.getCode());
		this.setErrmsg(errmsg);
		this.setKey(err.name());
	}
	
	public ServerException(ServerError err, String variable) {
		this(err.getCode(), err.getMsg(), err.name());
		setVariable(variable);
	}
	
	public ServerException(ServerError err) {
		this(err.getCode(), err.getMsg(),err.name());
	}

	public ServerException(Exception e, ServerError err) {
		this(e, err, err.getMsg());
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
