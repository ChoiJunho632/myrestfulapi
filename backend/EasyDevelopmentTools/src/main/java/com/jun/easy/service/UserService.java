package com.jun.easy.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jun.easy.bean.model.ApiWithBLOBs;
import com.jun.easy.bean.model.Data;
import com.jun.easy.bean.model.Project;
import com.jun.easy.bean.model.ProjectWithBLOBs;
import com.jun.easy.bean.model.User;
import com.jun.easy.exception.ServerError;
import com.jun.easy.exception.ServerException;
import com.jun.easy.mapper.UserServiceMapper;
import com.jun.easy.mapper.model.ApiMapper;
import com.jun.easy.mapper.model.DataMapper;
import com.jun.easy.mapper.model.ProjectMapper;
import com.jun.easy.mapper.model.UserMapper;
import com.jun.easy.util.CommonUtil;
import com.jun.easy.util.Config;
import com.jun.easy.util.CryptUtil;
import com.jun.easy.util.DataMap;
import com.jun.easy.util.ResponseData;

@Service
public class UserService {

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private ApiMapper apiMapper;
	
	@Autowired
	private ProjectMapper projectMapper;
	
	@Autowired
	private DataMapper dataMapper;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserServiceMapper userServiceMapper;
	
	@Autowired
	private ResponseData mResd;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData register(
			User user) throws ServerException {
		User tempUser = new User();
		tempUser.setAccount(user.getAccount());
		if((userServiceMapper.selectUserBySelective(tempUser)).size() > 0) throw new ServerException(ServerError.ERR_USER_ALREADY_REGISTERED_ACCOUNT);
		user.setInsertDate(new Date());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userMapper.insert(user);
		return mResd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData login(
			User user) throws ServerException {
		ResponseData resd = new ResponseData();
		User tempUser = new User();
		tempUser.setAccount(user.getAccount());
		List<User> tempUserList = userServiceMapper.selectUserBySelective(tempUser);
		if(tempUserList.size() > 0 && tempUserList.size() <= 1) {
			if(bCryptPasswordEncoder.matches(user.getPassword(), tempUserList.get(0).getPassword())) {
				resd.put("auth", CryptUtil.Encrypt(Config.CRYPT_KEY_USER, tempUserList.get(0).getUserId() + "&" + tempUserList.get(0).getAccount() + "&" + new Date().getTime()));
				resd.put("authLevel", Config.AUTH_LEVEL_USER_DEFAULT);
			}else throw new ServerException(ServerError.ERR_USER_UNMATCHED_PASSWORD);
		}else throw new ServerException(ServerError.ERR_USER_UNREGISTERED_ACCOUNT);
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData autoLogin(
			String cryptAuthString) throws ServerException {
		ResponseData resd = new ResponseData();
		String[] decryptAuthString = CryptUtil.Decrypt(Config.CRYPT_KEY_USER, cryptAuthString).split("&");
		User tempUser = new User();
		tempUser.setUserId(Integer.parseInt(decryptAuthString[0]));
		tempUser.setAccount(decryptAuthString[1]);
		List<User> tempUserList = userServiceMapper.selectUserBySelective(tempUser);
		if(tempUserList.size() > 0) {
			resd.put("auth", CryptUtil.Encrypt(Config.CRYPT_KEY_USER, tempUserList.get(0).getUserId() + "&" + tempUserList.get(0).getAccount() + "&" + new Date().getTime()));
			resd.put("authLevel", Config.AUTH_LEVEL_USER_DEFAULT);
		}else throw new ServerException(ServerError.ERR_USER_UNREGISTERED_ACCOUNT);
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData projectList(
			int clientId
			, int listOffset
			, int rowCnt) throws ServerException {
		ResponseData resd = new ResponseData();
		if(listOffset == Config.NONE) resd.put("projectList", userServiceMapper.projectList(clientId, true));
		else {
			resd.put("projectList", userServiceMapper.projectList(clientId, listOffset, rowCnt, false));
			resd.put("projectListCnt", userServiceMapper.projectListCnt(clientId));
		}
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void projectCreate(ProjectWithBLOBs project, int clientId) throws ServerException {
		project.setUserId(clientId);
		project.setInsertDate(new Date());
		if(projectMapper.insert(project) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void projectUpdate(ProjectWithBLOBs project, int clientId) throws ServerException {
		project.setUserId(clientId);
		project.setUpdateDate(new Date());
		if(projectMapper.updateByPrimaryKeySelective(project) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void projectDelete(int projectId, int clientId) throws ServerException {
		ProjectWithBLOBs project = new ProjectWithBLOBs();
		project.setProjectId(projectId);
		project.setUserId(clientId);
		project.setDeleteDate(new Date());
		if(projectMapper.updateByPrimaryKeySelective(project) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData apiList(
			int projectId
			, int listOffset
			, int rowCnt) throws ServerException {
		ResponseData resd = new ResponseData();
		resd.put("apiList", userServiceMapper.apiList(projectId, listOffset, rowCnt));
		resd.put("apiListCnt", userServiceMapper.apiListCnt(projectId));
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void apiCreate(ApiWithBLOBs api) throws ServerException {
		api.setInsertDate(new Date());
		if(apiMapper.insert(api) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void apiUpdate(ApiWithBLOBs api) throws ServerException {
		api.setUpdateDate(new Date());
		if(apiMapper.updateByPrimaryKeySelective(api) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void apiDelete(int apiId, int projectId) throws ServerException {
		ApiWithBLOBs api = new ApiWithBLOBs();
		api.setApiId(apiId);
		api.setProjectId(projectId);
		api.setDeleteDate(new Date());
		if(apiMapper.updateByPrimaryKeySelective(api) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void dataCreate(Data data, int clientId) throws ServerException {
		data.setUserId(clientId);
		data.setInsertDate(new Date());
		if(dataMapper.insert(data) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void dataUpdate(Data data) throws ServerException {
		data.setUpdateDate(new Date());
		if(dataMapper.updateByPrimaryKeyWithBLOBs(data) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void dataDelete(int dataId) throws ServerException {
		Data data = new Data();
		data.setDeleteDate(new Date());
		if(dataMapper.updateByPrimaryKeySelective(data) < 1) throw new ServerException(ServerError.ERR_COMMON_GENERAL_DATABASE);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData dataList(
		int cliendId) throws ServerException {
		ResponseData resd = new ResponseData();
		resd.put("dataList", userServiceMapper.dataList(cliendId));
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public HashMap<String, Object> insertDateCountForGraph(int cliendId) throws ServerException {
		ResponseData resd = new ResponseData();
		resd.put("projectApi", userServiceMapper.countProjectApiInsertDate(cliendId));
		resd.put("data", userServiceMapper.countDataInsertDate(cliendId));
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData findAccount(User user) throws ServerException {
		ResponseData resd = new ResponseData();
		List<User> userList = userServiceMapper.selectUserBySelective(user);
		if(userList.size() < 1) throw new ServerException(ServerError.ERR_USER_UNREGISTERED_ACCOUNT);
		else resd.put("account", userList.get(0).getAccount());
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData findPassword(User user) throws ServerException {
		ResponseData resd = new ResponseData();
		List<User> userList = userServiceMapper.selectUserBySelective(user);
		if(userList.size() < 1) throw new ServerException(ServerError.ERR_USER_UNREGISTERED_ACCOUNT);
		else {
			String tempPassword = CommonUtil.getRandomStringByOption(Config.RANDOM_PASSWORD_START_POINT, Config.RANDOM_PASSWORD_CNT, Config.RANDOM_PASSWORD_LENGTH);
			User tempUser = new User();
			tempUser.setUserId(userList.get(0).getUserId());
			tempUser.setPassword(bCryptPasswordEncoder.encode(tempPassword));
			userMapper.updateByPrimaryKeySelective(tempUser);
			resd.put("tempPassword", tempPassword);
		}
		return resd;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public ResponseData myInfoPasswordConfirm(
		User user
		, int userId) throws ServerException {
		ResponseData resd = new ResponseData();
		User tempUser = userMapper.selectByPrimaryKey(userId);
		if(!bCryptPasswordEncoder.matches(user.getPassword(), tempUser.getPassword())) throw new ServerException(ServerError.ERR_USER_UNMATCHED_PASSWORD);
		else {
			resd.put("userInfo", userMapper.selectByPrimaryKey(userId));
			return resd;
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void myInfoUpdate(User user) throws ServerException {
		user.setUpdateDate(new Date());
		userMapper.updateByPrimaryKeySelective(user);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void myInfoPasswordChange(User user) throws ServerException {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userMapper.updateByPrimaryKeySelective(user);
	}
	
}
