'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:DataCtrl
 * @description
 * # DataCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('DataCtrl', function ($scope, $state, AppConst, Util, Http, Modal) {

    var offset = 0;

    $scope.init = function() {
      $scope.data = {
        name : "",
        params : [{key : "", value : ""}]
      };
    };

    $scope.testInit = function(initParam) {
      $scope.init();
      if(!initParam) {
        $scope.projectList = [], $scope.mProject, $scope.mPrefix;
        $scope.apiList = [], $scope.mApi, $scope.hasApiListNext = false;
        $scope.dataList = [], $scope.mData;
      }
      Http.defaultHttp({
        method : "GET",
        uri : "user/project/list/whole"
      })
        .then(function(res) {
          if(res.projectList.length < 1) {
            alert(AppConst.getWarningMessage().NO_PROJECT);
            $state.go("project.create");
          }
          $scope.projectList = res.projectList;
          if(initParam) {
            for(var i in $scope.projectList) {
              if($scope.projectList[i].projectId == initParam.projectId) {
                $scope.mProject = $scope.projectList[i];
                $scope.mProject.prefixes = JSON.parse($scope.mProject.prefixes);
                for(var j in $scope.mProject.prefixes) {
                  if($scope.mProject.prefixes[j].name == initParam.prefixName) $scope.mPrefix = $scope.mProject.prefixes[j];
                }
              }
            }
          }else {
            $scope.mProject = $scope.projectList[0];
            $scope.mProject.prefixes = JSON.parse($scope.mProject.prefixes);
            $scope.mPrefix = $scope.mProject.prefixes[0];
          }
          Http.defaultHttp({
            method : "GET",
            uri : "user/api/list",
            param : {
              projectId : $scope.mProject.projectId,
              offset : offset,
              rowCnt : offset + AppConst.getPagingValue().COUNT_PER_PAGE
            }
          })
            .then(function(res) {
              $scope.apiList = res.apiList;
              $scope.mApi = initParam ? function() {
                for(var i in $scope.apiList) {
                  if($scope.apiList[i].apiId == initParam.apiId) return $scope.apiList[i];
                }
              }() : $scope.apiList[0];
              Http.defaultHttp({
                method : "GET",
                uri : "user/data/list"
              })
                .then(function(res) {
                  $scope.dataList = res.dataList;
                  $scope.mData = initParam ? function() {
                    for(var i in $scope.dataList) {
                      if($scope.dataList[i].dataId == initParam.dataId) return $scope.dataList[i];
                    }
                  }() : $scope.dataList[0];
                  setParamsAndHeaders($scope.mApi, $scope.mData);
                }, function(res) {Http.httpError(res)});
            }, function(res) {Http.httpError(res)});
        }, function(res) {Http.httpError(res)});
    };

    $scope.dataCreate = function(data) {
      if($scope.form.$invalid) return ;
      if(Util.blankBothKeyValueRemoveAndFindBlankKey(data.params)) {
        alert(AppConst.getCrudResultMessage("createDenBlank"));
        return ;
      }
      Http.defaultHttp({
        method : "POST",
        uri : "user/data/create",
        param : {
          name : data.name,
          dataValue : JSON.stringify(data.params)
        }})
        .then(function() {
          alert(AppConst.getCrudResultMessage("createSuc"));
          $state.go("data.test");
        }, function(res) {Http.httpError(res)});
    };

    $scope.paramKeyChange = function(param, index, isLast) {
      if(isLast && param.key !== "") $scope.data.params.push({key : "", value : ""});
      else {
        if(!param.key && !param.value) $scope.data.params.splice(index, 1);
      }
    };

    $scope.headerKeyChange = function(header, index, isLast) {
      if(isLast && header.key !== "") $scope.mApi.headers.push({key : "", value : ""});
      else {
        if(!header.key && !header.value) $scope.mApi.headers.splice(index, 1);
      }
    };

    $scope.projectSelectChange = function(mProject) {
      if(!angular.isArray(mProject.prefixes)) mProject.prefixes = JSON.parse(mProject.prefixes);
      $scope.mPrefix = mProject.prefixes[0];
      offset = 0;
      Http.defaultHttp({
        method : "GET",
        uri : "user/api/list",
        param : {
          projectId : mProject.projectId,
          offset : offset,
          rowCnt : offset + AppConst.getPagingValue().COUNT_PER_PAGE
        }
      })
        .then(function(res) {
          $scope.apiList = res.apiList;
          $scope.mApi = $scope.apiList[0];
          if($scope.mApi.isEditOnce) {
            $scope.mApi.url = $scope.mApi.uri;
            $scope.mApi.uri = "";
          }
          setParamsAndHeaders($scope.mApi, $scope.mData);
        }, function(res) {Http.httpError(res)})
    };

    $scope.apiSelectChange = function(mApi) {
      if($scope.mApi.isEditOnce) {
        $scope.mApi.url = $scope.mApi.uri;
        $scope.mApi.uri = "";
      }
      setParamsAndHeaders(mApi, $scope.mData);
    };

    $scope.dataSelectChange = function(mData) {
      setParamsAndHeaders($scope.mApi, mData);
    };

    $scope.paramInputDelete = function(index) {
      $scope.data.params.splice(index, 1);
    };

    $scope.headerInputDelete = function(index) {
      $scope.mApi.headers.splice(index, 1);
    };

    $scope.dataCreateCancel = function() {
      $scope.init();
    };

    $scope.testCancel = function() {
      $scope.testInit({
        projectId : $scope.mProject.projectId,
        apiId : $scope.mApi.apiId,
        dataId : $scope.mData.dataId,
        prefixName : $scope.mPrefix.name
      });
    };

    $scope.test = function() {
      Http.apiTestHttp(Util.setApiTestForm($scope.mApi.httpMethod, $scope.mApi.isEditOnce ? $scope.mApi.url : $scope.mPrefix.value + $scope.mApi.uri, $scope.data.params, $scope.mApi.headers))
        .then(function(res) {
          if(typeof (res.data) == "object") res.encData = "요청 파라미터 \n" + JSON.stringify($scope.data.params, null, 3) + "\n\n응답 값 \n" + JSON.stringify(res.data, null, 3);
          Modal.modalOpen("api_test", {
            api : $scope.mApi,
            res : res
          });
        }, function(res) {Http.httpError(res)});
    };

    function setParamsAndHeaders(api, data) {
      try {
        if(!angular.isArray(api.params)) {
          api.params = JSON.parse(api.params);
          api.headers = JSON.parse(api.headers);
        }
        if(!angular.isArray(data.dataValue)) data.dataValue = JSON.parse(data.dataValue);
        $scope.data.params = [];
        for(var i in api.params) {
          $scope.data.params.push(api.params[i]);
        }
        for(var i in data.dataValue) {
          $scope.data.params.push(data.dataValue[i]);
        }
        $scope.mApi.headers = [];
        for(var i in api.headers) {
          $scope.mApi.push(api.headers[i]);
        }
        Util.blankBothKeyValueRemoveAndFindBlankKey($scope.data.params);
        Util.blankBothKeyValueRemoveAndFindBlankKey($scope.mApi.headers);
        $scope.data.params.push({key : "", value : ""});
        $scope.mApi.headers.push({key : "", value : ""});
      }catch(e) {
        $scope.data.params = [{key : "", value : ""}];
        $scope.mApi = {};
        $scope.mApi.headers = [{key : "", value : ""}];
      }
    }
  });
