'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulprojectApp.controller:ProjectCtrl
 * @description
 * # ProjectCtrl
 * Controller of the myEasyRestfulprojectApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('ProjectCtrl', function ($scope, $state, $rootScope, AppConst, Modal, Util, Http) {
    $scope.createInit = function() {
      $scope.project = {
        name : "",
        prefixes : [
          {name : "", value : ""}
        ],
        description : ""
      };
      $scope.projectList = [];
    };

    $scope.init = function() {
      $scope.createInit();
      $scope.paging = Util.getPaging(null, AppConst.getPagingValue().INIT, true);
      Http.defaultHttp({
        method : "GET",
        uri : "user/project/list",
        param : {
          offset : $scope.paging.offset,
          rowCnt : $scope.paging.rowCnt
        }
      })
        .then(function(res) {
          if(res.projectList.length < 1) {
            alert(AppConst.getWarningMessage().NO_PROJECT);
            $state.go("project.create");
          }
          $scope.projectList = res.projectList;
          $scope.paging.itemCnt = res.projectListCnt;
          $scope.paging = Util.getPaging($scope.paging, AppConst.getPagingValue().INIT);
        }, function(res) {
          Http.httpError(res);
        });

      $scope.projectList = [];
    };

    $scope.prefixNameChange = function(prefix, isLast, index) {
      if(isLast && prefix.name !== "") $scope.project.prefixes.push({name : "", value : ""});
      else {
        if(!prefix.name && !prefix.value) $scope.project.prefixes.splice(index, 1);
      }
    };

    $scope.prefixInputDelete = function(index) {
      $scope.project.prefixes.splice(index, 1);
    };

    $scope.drawPlaceholder = function(isFirst, value) {
      if(isFirst) return value;
    };

    $scope.projectUpdate = function(project) {
      if(confirm(AppConst.crudConst.update(project.name, "project"))) {
        if(!angular.isArray(project.prefixes)) project.prefixes = JSON.parse(project.prefixes);
        Util.blankBothKeyValueRemoveAndFindBlankKey(project.prefixes);
        project.prefixes.push({name : "", value : ""});
        Modal.modalOpen("project_update", project);
      }
    };

    $scope.projectDelete = function(project) {
      if(confirm(AppConst.crudConst.delete(project.name, "project"))) {
        Http.defaultHttp({
          method : "GET",
          uri : "user/project/delete",
          param : {projectId : project.projectId}
        })
          .then(function() {
            alert(AppConst.getCrudResultMessage("deleteSuc"));
            $scope.init();
          }, function(res) {
            Http.httpError(res);
          });

      }
    };

    $scope.projectCreate = function(project) {
      if($scope.form.$invalid) return ;
      if(Util.blankBothKeyValueRemoveAndFindBlankKey(project.prefixes)) {
        alert(AppConst.getCrudResultMessage("createDenBlank"));
        return ;
      }
      var paramProject = angular.copy(project);
      paramProject.prefixes = JSON.stringify(project.prefixes);
      Http.defaultHttp({
        method : "POST",
        uri : "user/project/create",
        param : paramProject
      })
        .then(function() {
          alert(AppConst.getCrudResultMessage("createSuc"));
          $scope.projectList.push(project);
          $state.go("project.list");
        }, function(res) {
          Http.httpError(res);
        });
    };

    $scope.projectCreateCancel = function() {
      if(confirm(AppConst.crudConst.createCancel())) {
        $state.go("project.list");
      }
    };

    $scope.$on("$destroy", $rootScope.$on("project_update", function(e, p) {
      delete p.insertDate;
      delete p.updateDate;
      var paramProject = angular.copy(p);
      if(Util.blankBothKeyValueRemoveAndFindBlankKey(paramProject.prefixes)) {
        alert(AppConst.getCrudResultMessage("createDenBlank"));
        return ;
      }
      paramProject.prefixes = JSON.stringify(paramProject.prefixes);
      Http.defaultHttp({
        method : "POST",
        uri : "user/project/update",
        param : paramProject
      })
        .then(function() {
          for(var i in $scope.projectList) {
            if($scope.projectList[i].projectId === p.projectId) {
              $scope.projectList[i] = p;
              break;
            }
          }
          alert(AppConst.getCrudResultMessage("updateSuc"));
        }, function(res) {
          Http.httpError(res);
          $scope.init();
        });
    }));

    $scope.movePage = function(selectedValue) {
      if(typeof (selectedValue) == "number") selectedValue ++;
      $scope.paging = Util.getPaging($scope.paging, selectedValue);
      Http.defaultHttp({
        method : "GET",
        uri : "user/project/list",
        param : {
          offset : $scope.paging.offset,
          rowCnt : $scope.paging.rowCnt
        }
      })
        .then(function(res) {
          $scope.projectList = res.projectList;
          $scope.paging.itemCnt = res.projectListCnt;
          $scope.paging = Util.getPaging($scope.paging, $scope.paging.currentPage);
        }, function(res) {Http.httpError(res)});
    };

    $scope.itemIndex = function(index) {
      return (($scope.paging.currentPage - 1) * AppConst.getPagingValue().COUNT_PER_PAGE) + (($scope.paging.startCnt - 1) * AppConst.getPagingValue().COUNT_PER_PAGE) + (index + 1);
    }

  });
