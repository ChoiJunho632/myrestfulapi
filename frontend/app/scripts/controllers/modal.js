'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('ModalCtrl', function ($scope, $uibModalInstance, params, Util) {
    $scope.init = function() {
      $scope.wrap = {};
      $scope.wrap = params;
    };

    $scope.inputNameChange = function(obj, objKey, isLast, index, array, objExpresstion) {
      if(isLast && obj[objKey] !== "") array.push(objExpresstion);
      else {
        if((obj[objKey] == "" && obj.value) || (!obj[objKey] && !obj.value)) array.splice(index, 1);
      }
    };

    $scope.inputDelete = function(index, array) {
      array.splice(index, 1);
    };

    $scope.accordionOpen = function(sw, obj) {
      if (obj.resultKeyValues) return;
      else obj.resultKeyValues = Util.setParamsByDepthObj(obj.res);
    };

    $scope.addValue = function(obj, target) {
      target.push(obj);
    };

    $scope.ok = function() {
      if($scope.form) if($scope.form.$invalid) return ;
      $scope.$emit($scope.wrap.emit, $scope.wrap.value);
      $uibModalInstance.close($scope.wrap);
    };

    $scope.close = function() {
      $uibModalInstance.dismiss();
    }
  });
