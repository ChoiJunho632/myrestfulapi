'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:SidebarCtrl
 * @description
 * # SidebarCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('SidebarCtrl', function ($scope, $uibModalInstance) {
    $scope.sGroups = [
      {
        name : "프로젝트 관리",
        items : [
          {name : "프로젝트 목록", state : "project.list"},
          {name : "프로젝트 추가", state : "project.create"}]
      },
      {
        name : "API 관리",
        items : [
          {name : "API 목록", state : "api.list"},
          {name : "API 추가", state : "api.create({param1 : 1})"}]
      },
      {
        name : "데이터 관리 & 테스트",
        items : [
          {name : "데이터 생성", state : "data.create"},
          {name : "테스트", state : "data.test"}]
      }
    ];

    $scope.ok = function() {
      $uibModalInstance.close("result");
    }

    $scope.cancel = function() {
      $uibModalInstance.dismiss("reason");
    }
  });
