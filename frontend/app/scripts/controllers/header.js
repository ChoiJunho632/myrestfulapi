'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('HeaderCtrl', function ($scope, $rootScope, Aside, Util, Modal, Http, AppConst) {
    $scope.init = function() {
      $scope.apiTestResultCount = Util.getApiTestResultCount() ? Util.getApiTestResultCount() : 0;
    }

    $scope.openSidebar = function() {
      Aside.openSidebar();
    };

    $scope.logout = function() {
      $scope.$emit("userLogout");
    };

    $scope.apiTestResultClick = function() {
      $scope.apiTestResultCount = Util.clearApiTestResultCount();
      Modal.modalOpen("api_testResultList", {
        apiTestResultList : angular.copy(Util.getApiTestResultList())
      });
    };

    $scope.apiTestResultListLength = function() {
      return Util.getApiTestResultList().length;
    };

    $rootScope.$on("api_test", function(e, p) {
      Util.setApiTestResult({
        projectId : p.api.projectId,
        method : p.res.config.method,
        url : p.res.config.url,
        res : p.res.data,
        encErs : p.res.encData,
        status : p.res.data.status
      });
      $scope.apiTestResultCount = Util.getApiTestResultCount();
    });

    $rootScope.$on("api_testResultList", function(e, p) {
      var param = [];
      for(var i in p.apiTestResultList) {
        if(p.apiTestResultList[i].resultKeyValues) {
          var resultKeyValues = p.apiTestResultList[i].resultKeyValues;
          for(var j in resultKeyValues) {
            if(resultKeyValues[j].isChecked) param.push({key : resultKeyValues[j].key, value : resultKeyValues[j].value});
          }
        }
      }
      if(param.length > 0) {
        param.push({key : "", value : ""});
        Modal.modalOpen("data_create", {data : {params : param}});
      }
    });

    $rootScope.$on("data_create", function(e, p) {
      Http.defaultHttp({
        method : "POST",
        uri : "user/data/create",
        param : {
          name : p.data.name,
          dataValue : JSON.stringify(p.data.params)
        }
      })
        .then(function() {
          alert(AppConst.getCrudResultMessage("createSuc"));
        }, function(res) {Http.httpError(res)})
    });
  });
