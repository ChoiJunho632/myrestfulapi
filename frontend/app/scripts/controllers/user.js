'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('UserCtrl', function ($scope, $state, $filter, Http, Auth, AppConst) {
    $scope.init = function() {
      $scope.user =
      {
        account: "",
        password: "",
        passwordConfirm : "",
        name: "",
        email: ""
      };
      $scope.isNotBothPasswordMatch = false;
      $scope.form = {};
      $scope.form.$submitted = false;
    };

    $scope.infoInit = function() {
      $scope.myInfo = {};
    };

    $scope.register = function() {
      if($scope.form.$invalid || !$scope.user.password || !$scope.user.account) return ;
      else if(!angular.equals($scope.user.password, $scope.user.passwordConfirm)) {
        $scope.isNotBothPasswordMatch = true;
        return ;
      }else {
        $scope.isNotBothPasswordMatch = false;
        Http.defaultHttp({
          method : "POST",
          uri : "user/register",
          param : $scope.user
        })
          .then(function() {
            alert("회원가입 되었습니다.");
            $state.go("user.login");
          }, function(res) {
            Http.httpError(res);
          });
      }
    };

    $scope.login = function(user, isAutoLogin) {
      if(!user || !user.account || !user.password) return ;
      else if(!isAutoLogin) isAutoLogin = false;
      angular.merge(user, {isAutoLogin : isAutoLogin});
      Http.defaultHttp({
        method : "POST",
        uri : "user/login",
        param : user
      })
        .then(function(res) {
          if(res.auth) Auth.setAutoLogin(res.auth, res.authLevel);
          else Auth.checkAuth(res.authLevel);
          $state.go("home");
        }, function(res) {
          Http.httpError(res);
        });
    };

    $scope.findAccount = function(name, email) {
      if($scope.faForm.$invalid) return ;
      Http.defaultHttp({
        method : "POST",
        uri : "user/findAccount",
        param : {name : name, email : email}
      })
        .then(function(res) {
          $scope.fdAccount = res.account;
        }, function(res) {Http.httpError(res)});
    };

    $scope.findPassword = function(account, name, email) {
      if($scope.fpForm.$invalid) return ;
      Http.defaultHttp({
        method : "POST",
        uri : "user/findPassword",
        param : {account : account, name : name, email : email}
      })
        .then(function(res) {
          $scope.fpTempPassword = res.tempPassword;
        }, function(res) {Http.httpError(res)});
    };

    $scope.changePasswordBtnShow = function() {
      return !$scope.changePasswordForm && $scope.enterPassword;
    };

    $scope.enterPasswordForMyInfo = function(password) {
      if($scope.pifForm.$invalid) return ;
      Http.defaultHttp({
        method : "POST",
        uri : "user/myInfo/passwordConfirm",
        param : {password : password}
      })
        .then(function(res) {
          $scope.enterPassword = true;
          $scope.tempMyInfo = angular.copy(res.userInfo);
          $scope.myInfo.account = res.userInfo.account;
          $scope.myInfo.email = res.userInfo.email;
          $scope.myInfo.name = res.userInfo.name;
          $scope.myInfo.insertDate = $filter("date")(res.userInfo.insertDate, "yyyy-MM-dd");
          $scope.myInfo.updateDate = $filter("date")(res.userInfo.updateDate, "yyyy-MM-dd");
        }, function(res) {Http.httpError(res)});
    };

    $scope.infoUpdate = function(myInfo) {
      if(confirm(AppConst.crudConst.infoUpdate())) {
        var param = {}, keyArray = ["name", "email"], tempBoolean = false;
        for(var i in keyArray) {
          if($scope.tempMyInfo[keyArray[i]] != myInfo[keyArray[i]]) {
            tempBoolean = true;
            param[keyArray[i]] = myInfo[keyArray[i]];
          }
        }
        if(!tempBoolean) {
          alert(AppConst.getCrudResultMessage("infoUpdateDenNoChange"));
          return ;
        }
        Http.defaultHttp({
          method : "POST",
          uri : "user/myInfo/update",
          param : param
        })
          .then(function() {
            alert(AppConst.getCrudResultMessage("updateSuc"));
          }, function(res) {Http.httpError(res)});
      }
    };

    $scope.changePassword = function(changePasswordObject) {
      if($scope.cpForm.$invalid) return ;
      else if(!angular.equals(changePasswordObject.password, changePasswordObject.passwordConfirm)) {
        $scope.isPNotBothPasswordMatch = true;
        return;
      }else {
        $scope.isPNotBothPasswordMatch = false;
        Http.defaultHttp({
          method : "POST",
          uri : "user/myInfo/passwordChange",
          param : {password : changePasswordObject.password}
        })
          .then(function() {
            alert(AppConst.getCrudResultMessage("updateSuc"));
          }, function(res) {Http.httpError(res)})
      }
    };

  });
