'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:RootCtrl
 * @description
 * # RootCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('RootCtrl', function ($scope, $aside, $state, Auth, AppConst, Http, Util) {
    $scope.init = function() {
      $scope.labels = [], $scope.series = [], $scope.data = [], $scope.isDataExist = false;
      $scope.headerTmpl = "template/header.html";
      $scope.sideBarTmpl = "template/sidebar.html";
      $scope.footerTmpl = "template/footer.html";
    };

    $scope.mainGraphInit = function() {
      if($scope.isVisibleByAuth("user"))
        Http.defaultHttp({
          method : "GET",
          uri : "user/graphData"
        })
          .then(function(res) {
            Util.getStastisticGraphData(res);
            if(res.projectApi.length > 0 && res.data.length > 0) {
              var tempStastisticValue = Util.getStastisticGraphData(res);
              $scope.labels = tempStastisticValue.labels;
              $scope.data = tempStastisticValue.data;
              $scope.series = ["프로젝트", "API", "커스텀 데이터"];
              $scope.isDataExist = true;
            }else $scope.isDataExist = false;
          }, function(res) {Http.httpError(res)});
    };

    $scope.isVisibleByAuth = function(level) {
      var authLevel = Auth.getAuth().level ? AppConst.getAuthLevelNameByNumCode(Auth.getAuth().level) : AppConst.getAuthLevelNameByNumCode("guest");
      level = AppConst.getAuthLevelNameByNumCode(level);
      if(level > AppConst.getAuthLevelNameByNumCode("guest")) {
        if(authLevel >= level) return true;
      }else if(authLevel === level) return true;
      else return false;
    };

    $scope.$on("userLogout", function() {
      Http.defaultHttp({
        method : "GET",
        uri : "user/logout"
      })
        .then(function() {
          Auth.removeAuth();
          $state.go("home");
        }, function(res) {
          Http.httpError(res);
        });
    });

  });
