'use strict';

/**
 * @ngdoc function
 * @name myEasyRestfulApiApp.controller:ApiCtrl
 * @description
 * # ApiCtrl
 * Controller of the myEasyRestfulApiApp
 */
angular.module('myEasyRestfulApiApp')
  .controller('ApiCtrl', function ($scope, $state, $rootScope, $location, AppConst, Util, Http, Modal) {

    $scope.api = {
      name : "",
      httpMethod : "",
      prefix : {},
      uri : "",
      params : [
        {key : "", value : ""}
      ],
      headers : [
        {key : "", value : ""}
      ],
      isEditOnce : false
    };

    $scope.projectList = [
      {name : "", prefixes : [{name : "", value : ""}, {name : "", value : ""}], description : "", apiList : [{name : "", alias : ""}, {name : "", alias : ""}]}
    ];

    $scope.mProject = $scope.projectList[0];
    $scope.mPrefix = $scope.mProject.prefixes[0];

    $scope.httpMethods = [
      {key : "get", value : "get"},
      {key : "post", value : "post"},
      {key : "put", value : "put"},
      {key : "delete", value : "delete"},
      {key : "직접입력", value : "etc"}
    ];

    $scope.mHttpMethod = $scope.httpMethods[0];

    $scope.init = function(mProject) {
      $scope.paging = Util.getPaging(null, AppConst.getPagingValue().INIT, true);
      Http.defaultHttp({
        method : "GET",
        uri : "user/project/list/whole",
        param : {offset : -1}
      })
        .then(function(res) {
          if(res.projectList.length < 1) {
            alert(AppConst.getWarningMessage().NO_PROJECT);
            $state.go("project.create");
          }
          $scope.projectList = res.projectList;
          if(mProject)
            for(var i in res.projectList) {
              if(res.projectList[i].projectId == $scope.mProject.projectId) {
                $scope.projectList[i].prefixes = JSON.parse($scope.projectList[i].prefixes);
                $scope.mProject = res.projectList[i];
              }
            }
          else {
            $scope.projectList[0].prefixes = JSON.parse($scope.projectList[0].prefixes);
            $scope.mProject = $scope.projectList[0];
          }
          $scope.mPrefix = $scope.mProject.prefixes[0];
          Http.defaultHttp({
            method : "GET",
            uri : "user/api/list",
            param : {
              projectId : $scope.mProject.projectId,
              offset : $scope.paging.offset,
              rowCnt : $scope.paging.rowCnt
            }
          })
            .then(function(res) {
              if(res.apiList.length < 1 && ($location.path().indexOf("list") > -1)) if(confirm(AppConst.getWarningMessage().NO_API)) $state.go("api.create");
              $scope.mProject.apiList = res.apiList;
              $scope.paging.itemCnt = res.apiListCnt;
              $scope.paging = Util.getPaging($scope.paging, AppConst.getPagingValue().INIT);
            }, function(res) {Http.httpError(res);});
        }, function(res) {Http.httpError(res);});
    };

    $scope.projectSelectChange = function(project) {
      if(!angular.isArray(project.prefixes)) {
        project.prefixes = JSON.parse(project.prefixes);
        $scope.mPrefix = project.prefixes[0];
      }else $scope.mPrefix = project.prefixes[0];
      $scope.paging = Util.getPaging(null, AppConst.getPagingValue().INIT, true);
      Http.defaultHttp({
        method : "GET",
        uri : "user/api/list",
        param : {
          projectId : project.projectId,
          offset : $scope.paging.offset,
          rowCnt : $scope.paging.rowCnt
        }
      })
        .then(function(res) {
          if(res.apiList.length < 1) if(confirm(AppConst.getWarningMessage().NO_API)) $state.go("api.create");
          project.apiList = res.apiList;
          $scope.paging.itemCnt = res.apiListCnt;
          $scope.paging = Util.getPaging($scope.paging, AppConst.getPagingValue().INIT);
        }, function(res) {Http.httpError(res)});
    };

    $scope.prefixSelectChange = function(prefix) {
      $scope.mPrefix = prefix;
    };

    $scope.httpMethodSelectChange = function(mHttpMethod) {
      $scope.mHttpMethod = mHttpMethod;
    };

    $scope.apiUpdate = function(api) {
      if(confirm(AppConst.crudConst.update(api.name, "API"))) {
        if(!angular.isArray(api.params)) api.params = JSON.parse(api.params);
        if(!angular.isArray(api.headers)) api.headers = JSON.parse(api.headers);
        Util.blankBothKeyValueRemoveAndFindBlankKey(api.params);
        Util.blankBothKeyValueRemoveAndFindBlankKey(api.headers);
        api.params.push({key : "", value : ""});
        api.headers.push({key : "", value : ""});
        var tempHttpMethod = {}, isEtc = true;
        for(var i in $scope.httpMethods) {
          if ($scope.httpMethods[i].value == api.httpMethod) {
            tempHttpMethod = $scope.httpMethods[i];
            isEtc = false;
          }
        }
        if(isEtc) {
          $scope.httpMethods.push({key : api.httpMethod, value : api.httpMethod});
          tempHttpMethod = $scope.httpMethods[$scope.httpMethods.length-1];
        }
        Modal.modalOpen("api_update", {
          project : $scope.mProject,
          api : api,
          httpMethods : $scope.httpMethods,
          httpMethod : tempHttpMethod,
          prefixes : $scope.mProject.prefixes,
          prefix : $scope.mProject.prefixes[0]});
      }
    };

    $scope.apiDelete = function(api) {
      if(confirm(AppConst.crudConst.delete(api.name, "API"))) {
        Http.defaultHttp({
          method : "GET",
          uri : "user/api/delete",
          param : {projectId : $scope.mProject.projectId, apiId : api.apiId}
        })
          .then(function() {
            alert(AppConst.getCrudResultMessage("deleteSuc"));
            $scope.init($scope.mProject);
          }, function(res) {Http.httpError(res)});
      }
    };

    $scope.apiTest = function(api) {
      if(!angular.isArray(api.params)) api.params = JSON.parse(api.params);
      if(!angular.isArray(api.headers)) api.headers = JSON.parse(api.headers);
      Http.apiTestHttp(Util.setApiTestForm(api.httpMethod, api.isEditOnce ? api.uri : $scope.mPrefix.value + api.uri, api.params, api.headers))
        .then(function(res) {
          // if(typeof (res.data) == "object") res.data = res.config.params ? "요청 파라미터 \n" + JSON.stringify(res.config.params, null, 3) + "\n\n응답 값 \n" + JSON.stringify(res.data, null, 3) : "요청 파라미터 \n" + JSON.stringify(res.config.data, null, 3) + "\n\n응답 값 \n" + JSON.stringify(res.data, null, 3);
          if(typeof (res.data) == "object") res.encData = "요청 파라미터 \n" + JSON.stringify(api.params, null, 3) + "\n\n응답 값 \n" + JSON.stringify(res.data, null, 3);
          Modal.modalOpen("api_test", {
            api : api,
            res : res
          });
        });
    };

    $scope.apiUpdateNameAlias = function(api) {
      if (confirm(AppConst.crudConst.changeAlias(api.name, api.alias, api.alias ? "수정" : "추가", "API"))) {

      }
    };

    $scope.paramKeyChange = function(param, index, isLast) {
      if(isLast && param.key !== "") $scope.api.params.push({key : "", value : ""});
      else {
        if(!param.key && !param.value) $scope.api.params.splice(index, 1);
      }
    };

    $scope.paramInputDelete = function(index) {
      $scope.api.params.splice(index, 1);
    };

    $scope.headerKeyChange = function(header, index, isLast) {
      if(isLast && header.key !== "") $scope.api.params.push({key : "", value : ""});
      else {
        if(!header.key && !header.value) $scope.api.params.splice(index, 1);
      }
    };

    $scope.headerInputDelete = function(index) {
      $scope.api.headers.splice(index, 1);
    };

    $scope.apiCreate = function(api) {
      console.log($scope.form);
      if($scope.form.$invalid) return ;
      var paramApi = angular.copy(api);
      if(angular.equals(paramApi.httpMethod, "")) paramApi.httpMethod = $scope.mHttpMethod.value;
      paramApi.isEditOnce = api.isEditOnce;
      paramApi.prefix = $scope.mPrefix;
      if(Util.blankBothKeyValueRemoveAndFindBlankKey(paramApi.params) || Util.blankBothKeyValueRemoveAndFindBlankKey(paramApi.headers)) {
        alert(AppConst.getCrudResultMessage("createDenBlank"));
        return ;
      }
      paramApi.projectId = $scope.mProject.projectId;
      paramApi.params = JSON.stringify(paramApi.params);
      paramApi.headers = JSON.stringify(paramApi.headers);
      Http.defaultHttp({
        method : "POST",
        uri : "user/api/create",
        param : paramApi
      })
        .then(function() {
          alert(AppConst.getCrudResultMessage("createSuc"));
          $state.go("api.list");
        }, function(res) {Http.httpError(res)});
    };

    $scope.apiCreateCancel = function() {
      if(confirm(AppConst.crudConst.createCancel())) {
        $state.go("api.list");
      }
    };

    $scope.$on("$destroy", $rootScope.$on("api_update", function(e, p) {
      if(Util.blankBothKeyValueRemoveAndFindBlankKey(p.api.params) || Util.blankBothKeyValueRemoveAndFindBlankKey(p.api.headers)) {
        alert(AppConst.getCrudResultMessage("createDenBlank"));
        return ;
      }
      var paramApi = {
        apiId : p.api.apiId,
        projectId : p.project.projectId,
        name : p.api.name,
        httpMethod : p.httpMethod.value == "etc" ? p.api.httpMethod : p.httpMethod.value,
        // uri : p.isUrlEditOnce ? p.api.uri : p.prefix.value + p.api.uri,
        uri : p.api.uri,
        isEditOnce : p.api.isEditOnce,
        params : JSON.stringify(p.api.params),
        headers : JSON.stringify(p.api.headers)
      };
      Http.defaultHttp({
        method : "POST",
        uri : "user/api/update",
        param : paramApi
      })
        .then(function() {
          $scope.init($scope.mProject);
          alert(AppConst.getCrudResultMessage("updateSuc"));
        }, function(res) {Http.httpError(res)});
    }));

    $scope.movePage = function(selectedValue) {
      if(typeof (selectedValue) == "number") selectedValue ++;
      $scope.paging = Util.getPaging($scope.paging, selectedValue);
      Http.defaultHttp({
        method : "GET",
        uri : "user/api/list",
        param : {
          projectId : $scope.mProject.projectId,
          offset : $scope.paging.offset,
          rowCnt : $scope.paging.rowCnt
        }
      })
        .then(function(res) {
          $scope.mProject.apiList = res.apiList;
          $scope.paging.itemCnt = res.apiListCnt;
          $scope.paging = Util.getPaging($scope.paging, AppConst.getPagingValue().INIT);
        }, function(res) {Http.httpError(res)});
    };

    $scope.itemIndex = function(index) {
      return (($scope.paging.currentPage - 1) * AppConst.getPagingValue().COUNT_PER_PAGE) + (($scope.paging.startCnt - 1) * AppConst.getPagingValue().COUNT_PER_PAGE) + (index + 1);
    };

  });
