'use strict';

/**
 * @ngdoc overview
 * @name myEasyRestfulApiApp
 * @description
 * # myEasyRestfulApiApp
 *
 * Main module of the application.
 */
var myEasyRestfulApiApp = angular
  .module('myEasyRestfulApiApp', [
    'ngCookies',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ngAside',
    'ngStorage',
    'ngCookies',
    'chart.js'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
    $httpProvider.defaults.headers.delete = {};

    $urlRouterProvider.otherwise("/home");

    $stateProvider
      .state("home", {
        url : "/home?param1",
        params: {
          param1: null
        },
        templateUrl : "views/main.html"
      })
      .state("api", {
        abstract : true,
        url : "/api",
        template: "<ui-view/>",
        authenticate : "user"
      })
      .state("api.list", {
        url : "/list",
        controller : "ApiCtrl",
        templateUrl : "views/api_list.html",
        authenticate : "user"
      })
      .state("api.create", {
        url : "/create",
        controller : "ApiCtrl",
        templateUrl : "views/api_create.html",
        authenticate : "user"
      })
      .state("user", {
        abstract : true,
        url : "/user",
        template : "<ui-view/>"
      })
      .state("user.register", {
        url : "/register",
        controller : "UserCtrl",
        templateUrl : "views/user_register.html",
        authenticate : "guest"
      })
      .state("user.login", {
        url : "/login",
        controller : "UserCtrl",
        templateUrl : "views/user_login.html",
        authenticate : "guest"
      })
      .state("user.info", {
        url : "/info",
        controller : "UserCtrl",
        templateUrl : "views/user_info.html",
        authenticate : "user"
      })
      .state("user.find", {
        abstract : true,
        url : "/find",
        template : "<ui-view/>"
      })
      .state("user.find.account", {
        url : "/account",
        controller : "UserCtrl",
        templateUrl : "views/user_find_account.html",
        authenticate : "guest"
      })
      .state("user.find.password", {
        url : "/password",
        controller : "UserCtrl",
        templateUrl : "views/user_find_password.html",
        authenticate : "guest"
      })
      .state("project", {
        abstract : true,
        authenticate : "user",
        url : "/project",
        template : "<ui-view/>"
      })
      .state("project.create", {
        url : "/create",
        controller : "ProjectCtrl",
        templateUrl : "views/project_create.html",
        authenticate : "user"
      })
      .state("project.list", {
        url : "/list",
        controller : "ProjectCtrl",
        templateUrl : "views/project_list.html",
        authenticate : "user"
      })
      .state("data", {
        abstract : true,
        authenticate : "user",
        url : "/data",
        template : "<ui-view/>"
      })
      .state("data.create", {
        url : "/create",
        controller : "DataCtrl",
        templateUrl : "views/data_create.html",
        authenticate : "user"
      })
      .state("data.test", {
        url : "/test",
        controller : "DataCtrl",
        templateUrl : "views/data_test.html",
        authenticate : "user"
      })
  })
  .run(function($rootScope, $cookieStore, $state, $window, Auth, Http, AppConst) {
    if($cookieStore.get("auth")) {
      Http.defaultHttp({
        method : "POST",
        uri : "user/autoLogin",
        param : {auth : $cookieStore.get("auth").value}
      })
        .then(function() {
          Auth.checkAuth();
        }, function(res) {
          Http.httpError(res);
          $cookieStore.remove("auth");
          Auth.checkAuth();
        });
    }

    $rootScope.$on("$stateChangeStart", function(e, toState, toParam, fromState, fromParam) {
      if(toState.authenticate) {
        var nLevel = toState.authenticate, mLevel = Auth.getAuth().level;
        if(nLevel === "guest" && mLevel !== "guest" || AppConst.getAuthLevelNameByNumCode(mLevel) < AppConst.getAuthLevelNameByNumCode(nLevel)) $state.go("home");
        }
    });
  })
  .directive('drEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if(event.which === 13 && event.type == "keydown") {
          scope.$apply(function (){
            scope.$eval(attrs.drEnter);
          });

          event.preventDefault();
        }
      });
    };
  });
