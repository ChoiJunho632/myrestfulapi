'use strict';

/**
 * @ngdoc service
 * @name myEasyRestfulApiApp.const
 * @description
 * # const
 * Service in the myEasyRestfulApiApp.
 */
angular.module('myEasyRestfulApiApp')
  .service('AppConst', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.getPagingValue = function() {
      const COUNT_PER_PAGE = 10, COUNT_PER_BLOCK = 5, NONE = -1, INIT = 1;
      return {
        NONE : NONE,
        COUNT_PER_PAGE : COUNT_PER_PAGE,
        COUNTER_PER_BLOCK : COUNT_PER_BLOCK,
        INIT : INIT
      }
    };

    this.getWarningMessage = function() {
      const NO_PROJECT = "아직 프로젝트가 하나도 없습니다. \n프로젝트를 만들기 위해 이동합니다.", NO_API = "해당 프로젝트에 등록된 API가 없습니다. \nAPI를 등록하기 위해 이동하시겠습니까?";
      return {
        NO_PROJECT : NO_PROJECT,
        NO_API : NO_API
      }
    };

    var urlPrefix = "http://localhost:18080/EasyDevelopmentTools/";

    this.getUrlPrefix = function() {
      return urlPrefix;
    };

    var authLevel =
    {
      guest : 0,
      user : 100,
      admin : 200
    };

    this.getAuthLevelNameByNumCode = function(param) {
      return authLevel[param];
    };

    var crudResultMessage =
    {
      createSuc : "추가되었습니다.",
      updateSuc : "수정되었습니다.",
      deleteSuc : "삭제되었습니다.",
      createDenBlank : "모든 폼을 채워주세요.",
      infoUpdateDenNoChange : "변경 사항이 없습니다."
    };

    this.getCrudResultMessage = function(param) {
      return crudResultMessage[param];
    };

    this.crudConst = {
      update : function(name, type) {
        return "\"" + name + "\" " + type + "를 수정하시겠습니까?";
      },
      delete : function(name, type) {
        return "\"" + name + "\" " + type + "를 삭제하시겠습니까?";
      },
      changeAlias : function(name, alias, sw, type) {
        return "\"" + name + (alias ? "(" + alias + ")" : "") + "\" " + type + "의 별명을 " + sw + " 하시겠습니까?";
      },
      createCancel : function() {
        return "등록을 취소 하시겠습니까?";
      },
      infoUpdate : function() {
        return "내 정보를 수정하시겠습니까?";
      }
    }
  });
