'use strict';

/**
 * @ngdoc service
 * @name myEasyRestfulApiApp.modal
 * @description
 * # modal
 * Service in the myEasyRestfulApiApp.
 */
angular.module('myEasyRestfulApiApp')
  .service('Modal', function ($uibModal) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var modalObj = {};

    var setModalObj = function(params, template, type) {
      var tempParams = {
        key : "",
        value : params,
        emit : template
      };
      switch(type) {
        case "simple" :
          modalObj = {

          };
        default :
          modalObj = {
            backdrop : false,
            keyboard : false,
            size : "modal-md",
            templateUrl : "template_modal/" + template + ".html",
            controller : "ModalCtrl",
            resolve : {
              params : function() {
                return tempParams
              }
            }
          };
      }
    }

    this.modalOpen = function(templateUrl, params) {
      setModalObj(params, templateUrl);
      $uibModal.open(modalObj)
        .result.then(function(wrap) {

      }, function() {

      });
    }
  });
