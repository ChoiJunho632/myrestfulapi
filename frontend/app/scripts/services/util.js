'use strict';

/**
 * @ngdoc service
 * @name myEasyRestfulApiApp.util
 * @description
 * # util
 * Service in the myEasyRestfulApiApp.
 */
angular.module('myEasyRestfulApiApp')
  .service('Util', function ($q, $sessionStorage, AppConst) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var arrayLoop = function(array, tempKeySet, index) {
      var isNotBlank = false;
      for(var i in tempKeySet) {
        if(array[index][tempKeySet[i]] != "") {
          isNotBlank = true;
          break;
        }
      }
      if(!isNotBlank) {
        array.splice(index, 1);
        if(array.length-1 >= index) arrayLoop(array, tempKeySet, index);
      }else if(array.length-1 >= index+1) arrayLoop(array, tempKeySet, index+1);
    };

    this.blankBothKeyValueRemoveAndFindBlankKey = function(array) {
      if(array.length < 1) return ;
      var tempKeySet = window.Object.keys(array[0]);
      for(var i in tempKeySet) {
        if(tempKeySet[i] == "$$hashKey") tempKeySet.splice(i, 1);
      }
      var isFindBalnkKey = false;

      for(var i in array) {
        if(array[i][tempKeySet[0]] == "" && array[i][tempKeySet[1]] != "") {
          isFindBalnkKey = true;
          break;
        }
      }
      arrayLoop(array, tempKeySet, 0);
      return isFindBalnkKey;
    };

    // block, currntPage, pageCnt, leftBtn, rightBtn
    this.getPaging = function(pagingInfo, selectedValue, isInit) {
      var pagingInfo = pagingInfo;
      var pv = AppConst.getPagingValue();

      if(isInit) {
        pagingInfo = {
          currentPage : pv.NONE,
          currentBlock : pv.NONE,
          startCnt : pv.NONE,
          itemCnt : pv.NONE,
          leftBtn : false,
          rightBtn : false,
          offset : pv.NONE,
          rowCnt : pv.NONE,
          pageCnt : pv.NONE
        };
        pagingInfo.currentPage = pv.INIT;
        pagingInfo.currentBlock = pv.INIT;
        pagingInfo.startCnt = pv.INIT;
        pagingInfo.itemCnt = pv.INIT;
        pagingInfo.offset = (pagingInfo.currentPage - 1) * pv.COUNT_PER_PAGE;
        pagingInfo.rowCnt = pv.COUNT_PER_PAGE;
        pagingInfo.pageCnt = pv.INIT;
        return pagingInfo;
      }


      switch(selectedValue) {
        case "left" :
          pagingInfo.currentBlock --;
          pagingInfo.currentPage = 5;
              break;
        case "right" :
          pagingInfo.currentBlock ++;
          pagingInfo.currentPage = 1;
              break;
        default : pagingInfo.currentPage = selectedValue;
      }


      pagingInfo.startCnt = ((pagingInfo.currentBlock) - 1) * pv.COUNTER_PER_BLOCK + 1;
      pagingInfo.offset = ((pagingInfo.currentPage - 1) + (pagingInfo.startCnt - 1)) * pv.COUNT_PER_PAGE;
      pagingInfo.rowCnt = pv.COUNT_PER_PAGE;
      pagingInfo.leftBtn = pagingInfo.currentBlock >= 2;
      pagingInfo.rightBtn = getTotalBlockCnt(getTotalPageCnt(pagingInfo.itemCnt)) - pagingInfo.currentBlock >= 1;
      pagingInfo.pageCnt = getTotalPageCnt(pagingInfo.itemCnt) - (pagingInfo.startCnt - 1) > pv.COUNTER_PER_BLOCK ? new Array(pv.COUNTER_PER_BLOCK) : new Array(getTotalPageCnt(pagingInfo.itemCnt) - (pagingInfo.startCnt - 1));
      return pagingInfo;

      function getTotalPageCnt(itemCnt) {
        return itemCnt % pv.COUNT_PER_PAGE != 0 ? parseInt(itemCnt / pv.COUNT_PER_PAGE) + 1 : itemCnt / pv.COUNT_PER_PAGE;
      }

      function getTotalBlockCnt(pageCnt) {
        return pageCnt % pv.COUNTER_PER_BLOCK != 0 ? parseInt(pageCnt / pv.COUNTER_PER_BLOCK) + 1 : pageCnt / pv.COUNTER_PER_BLOCK;
      }
    };

    this.setApiTestForm = function(method, url, params, headers) {
      var tempApiTestFormObj = {
        method : method.value ? method.value : method,
        url : url,
        params : {},
        headers : {}
      };
      for(var i in params) {
        tempApiTestFormObj.params[params[i].key] = params[i].value;
      }
      for(var i in headers) {
        tempApiTestFormObj.headers[headers[i].key] = headers[i].value;
      }
      return tempApiTestFormObj;
    };

    this.setApiTestResult = function(result) {
      if(!$sessionStorage.apiTestResultList) {
        $sessionStorage.apiTestResultList = [];
        $sessionStorage.apiTestResultList.push(result);
        $sessionStorage.apiTestResultListCount = 1;
      }else {
        $sessionStorage.apiTestResultList.push(result);
        $sessionStorage.apiTestResultListCount ++;
      }
    };

    this.getApiTestResultList = function() {
      return $sessionStorage.apiTestResultList ? $sessionStorage.apiTestResultList : [];
    };

    this.getApiTestResultCount = function() {
      return $sessionStorage.apiTestResultListCount;
    };

    this.clearApiTestResultCount = function() {
      return $sessionStorage.apiTestResultListCount = 0;
    };

    this.setParamsByDepthObj = function(obj) {
      var param = [];
      check(obj);
      function addParam(row, key, value) {
        if(typeof(value) != "object" || value == null) {
          for(var i in param) {
            if(param[i].key == key && param[i].value == value) return ;
          }
          param.push({key : key, value : value});
        }else check(row[key]);
      }

      function check(obj) {
        var tempKeySet = Object.keys(obj);
        for(var i in tempKeySet) {
          addParam(obj, tempKeySet[i], obj[tempKeySet[i]]);
        }
      }
      return param;
    };

    this.getStastisticGraphData = function(res) {
      var projectApiRawList = res.projectApi, dataRawList = res.data;
      var data = [], labels = [];
      var isPExist = false, isAExist = false;
      for(var i in projectApiRawList) {
        for(var j in labels) {
          if(projectApiRawList[i].apiInsertDate) {
            if(projectApiRawList[i].apiInsertDate == labels[j]) isAExist = true;
          }else isAExist = true;
          if(projectApiRawList[i].projectInsertDate == labels[j]) {
            isPExist = true;
            break;
          }
        }
        if(!isPExist && !isAExist && projectApiRawList[i].projectInsertDate != projectApiRawList[i].apiInsertDate) {
          labels.push(projectApiRawList[i].projectInsertDate);
          labels.push(projectApiRawList[i].apiInsertDate);
        }else if(!isPExist) labels.push(projectApiRawList[i].projectInsertDate);
        else if(!isAExist) labels.push(projectApiRawList[i].apiInsertDate);
        isPExist = false;
        isAExist = false;
      }
      isPExist = false;
      for(var i in dataRawList) {
        for(var j in labels) {
          if(dataRawList[i].dataInsertDate == labels[j]) {
            isPExist = true;
            break;
          }
        }
        if(!isPExist) labels.push(dataRawList[i].dataInsertDate);
      }
      labels.sort();
      var tempArray = [];
      for(var i=0; i<labels.length; i++) {
        tempArray.push(0);
      }
      for(var i=0; i<3; i++) {
        data[i] = angular.copy(tempArray);
      }
      for(var i in labels) {
        for(var j in projectApiRawList) {
          if(labels[i] == projectApiRawList[j].projectInsertDate) data[0][i] += 1;
          if(projectApiRawList[j].apiInsertDate && labels[i] == projectApiRawList[j].apiInsertDate) data[1][i] += 1;
        }
        for(var j in dataRawList) {
          if(labels[i] == dataRawList[j].dataInsertDate) data[2][i] = dataRawList[j].dataCount;
        }
      }
      return {labels : labels, data : data};
    };
  });
