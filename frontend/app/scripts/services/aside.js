'use strict';

/**
 * @ngdoc service
 * @name myEasyRestfulApiApp.aside
 * @description
 * # aside
 * Service in the myEasyRestfulApiApp.
 */
angular.module('myEasyRestfulApiApp')
  .service('Aside', function ($aside) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.openSidebar = function() {
      $aside.open({
        templateUrl : "template/sidebar.html",
        controller : "SidebarCtrl",
        placement : "left",
        size : "sm"
      })
        .result.then(function(res) {
      }, function(res) {
        
      });
    };
  });
