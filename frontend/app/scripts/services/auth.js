'use strict';

/**
 * @ngdoc service
 * @name myEasyRestfulApiApp.auth
 * @description
 * # auth
 * Service in the myEasyRestfulApiApp.
 */
angular.module('myEasyRestfulApiApp')
  .service('Auth', function ($cookieStore, $sessionStorage) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var auth =
    {
      value : "",
      level : "guest",
      expire : ""
    };

    this.getAuth = function() {
      return $sessionStorage.auth ? $sessionStorage.auth : auth;
    };

    this.removeAuth = function() {
      $sessionStorage.$reset();
      if($cookieStore.get("auth")) $cookieStore.remove("auth");
      this.checkAuth();
    }

    this.checkAuth = function(level) {
      if(level) {
        $sessionStorage.auth = {
          value: "",
          level: level,
          expire: "/"
        };
      }else $sessionStorage.auth = $cookieStore.get("auth") ? $cookieStore.get("auth") : auth;
    };

    this.setAutoLogin = function(auth, level) {
      $cookieStore.put("auth", {value : auth, level : level, expire : "/"});
      this.checkAuth();
    };

  });
