'use strict';

/**
 * @ngdoc service
 * @name myEasyRestfulApiApp.Http
 * @description
 * # Http
 * Service in the myEasyRestfulApiApp.
 */
angular.module('myEasyRestfulApiApp')
  .service('Http', function ($http, $q, $httpParamSerializerJQLike, $state, AppConst, Auth) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.defaultHttp = function(params) {
      var tempHeaders = {
        "Content-Type" : "application/x-www-form-urlencoded"
      };
      if(params.headers) tempHeaders = setHeaders(params.headers);
      var defferd = $q.defer();
      var mHttp = {
        method : params.method,
        url : AppConst.getUrlPrefix() + params.uri,
        headers : tempHeaders,
        withCredentials: true
      };
      params.method === "GET" ? function() {mHttp.params = params.param;}() : function() {mHttp.data = $httpParamSerializerJQLike(params.param);}();
      $http(mHttp)
        .then(function(res) {
          if(res.data.code == 0) defferd.resolve(res.data.data);
          else {
            defferd.reject(res);
            if(res.data.code == 257) {
              Auth.removeAuth();
              $state.go("home");
            }
          }
        }, function(res) {defferd.reject(res);});
      return defferd.promise;
    };

    this.httpError = function(res) {
      if(res.data != null) alert(res.data.errmsg);
      else alert("서버에 문제가 발생했습니다. 관리자에게 문의해 주세요.");
    };

    this.apiTestHttp = function(params) {
      var tempHeaders = {
        "Content-Type" : "application/x-www-form-urlencoded"
      };
      if(angular.isArray(params.headers)) tempHeaders = setHeaders(params.headers);
      var defferd = $q.defer();
      var mHttp = {
        method : params.method,
        url : params.url,
        headers : tempHeaders,
        withCredentials: true
      };
      params.method.toUpperCase() == "GET" ? function() {mHttp.params = params.params;}() : function() {mHttp.data = $httpParamSerializerJQLike(params.params);}();
      $http(mHttp)
        .then(function(res) {defferd.resolve(res)}, function(res) {defferd.resolve(res)});
      return defferd.promise;
    };

    function setHeaders(headers) {
      var tempHeaders = {};
      var tempHeaderKeys = window.Object.keys(headers);
      for(var i in tempHeaderKeys) {
        tempHeaders[tempHeaderKeys[i]] = headers[tempHeaderKeys[i]];
      }
      return tempHeaders;
    }
  });
