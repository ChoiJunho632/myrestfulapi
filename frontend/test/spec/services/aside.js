'use strict';

describe('Service: aside', function () {

  // load the service's module
  beforeEach(module('myEasyRestfulApiApp'));

  // instantiate service
  var aside;
  beforeEach(inject(function (_aside_) {
    aside = _aside_;
  }));

  it('should do something', function () {
    expect(!!aside).toBe(true);
  });

});
